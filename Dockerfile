FROM debian:buster-slim


LABEL authors="Patrick Jentsch <p.jentsch@uni-bielefeld.de>, Stephan Porada <porada@posteo.de>"


ENV LANG=C.UTF-8


RUN apt-get update \
 && apt-get install --no-install-recommends --yes \
      imagemagick \
      procps \
      wget \
 && mv /etc/ImageMagick-6/policy.xml /etc/ImageMagick-6/policy.xml.bak


# Install the File setup pipeline and it's dependencies #
## Install pyFlow ##
ENV PYFLOW_VERSION=1.1.20
RUN wget --no-check-certificate --quiet \
      "https://github.com/Illumina/pyflow/releases/download/v${PYFLOW_VERSION}/pyflow-${PYFLOW_VERSION}.tar.gz" \
 && tar -xzf "pyflow-${PYFLOW_VERSION}.tar.gz" \
 && cd "pyflow-${PYFLOW_VERSION}" \
 && apt-get install --no-install-recommends --yes \
      python2.7 \
 && python2.7 setup.py build install \
 && cd - > /dev/null \
 && rm -r "pyflow-${PYFLOW_VERSION}" "pyflow-${PYFLOW_VERSION}.tar.gz"


RUN rm -r /var/lib/apt/lists/*


## Install Pipeline ##
COPY file-setup /usr/local/bin


ENTRYPOINT ["file-setup"]
CMD ["--help"]
